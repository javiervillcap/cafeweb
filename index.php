<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>I LOVE BOOS</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
     <div class="container">
   <div class="row">
    <h1>Empleados</h1>
   </div>
   <div class="row">
    <form id="registro" name="registro"method="POST" action="guarda.php" autocomplete="off">
    <div class="mb-3">
        <label for="nombre" class="form-label">Nombre</label>
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="IntroduceTuNombre" autofocus required>
    </div>
    
    <div class="mb-3">
        <label for="Telefono" class="form-label">Telefono</label>
        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="IntroduceTuTelefono" required>
    </div>
    <div class="mb-3">
        <label for="fechanacimiento" class="form-label">Fecha de nacimiento</label>
        <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Introduce la fecha de nacimiento" required>
    </div>
        <div class="mb-3">
        <label for="fechanacimiento">Estado civil</label>
        <select id="estado_civil" name="estado_civil" class="form-control" required>
        <option value="soltero">Soltero</option>
        <option value="casado">Casado</option>
        <option value="otro">Otro</option>
        </select>
    </div>
    <div>
      <button class="btn btn-primary" id="guarda" name="guarda"type="submit">Guarda</button>
    </div>
  </form>
  
    </div>
  </div>
    <script src="jquery-3.4.1.min.js"></script>
    <script src="bootstrap.min.js"></script>
  </body>
</html>
